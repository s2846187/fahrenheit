package nl.utwente.di.Fahrenheit;

public class Converter {
    double convertTemperature(String temperature){
        return (Integer.parseInt(temperature)*1.8 + 32);
    }
}
